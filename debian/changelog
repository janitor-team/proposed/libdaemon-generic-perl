libdaemon-generic-perl (0.85-2) UNRELEASED; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Damyan Ivanov <dmn@debian.org>  Sun, 03 Dec 2017 22:43:30 +0000

libdaemon-generic-perl (0.85-1) unstable; urgency=medium

  * Add debian/upstream/metadata.
  * Import upstream version 0.85.
  * Drop 0001-Fix-a-race-condition-in-the-test-suite-waiting-for-d.patch
    (merged upstream).
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Jul 2017 14:46:32 +0200

libdaemon-generic-perl (0.84-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Add patch to fix race condition in test suite.
    Thanks to Niko Tyni for the patch. (Closes: #834960)
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.
  * Bump debhelper compatibility level to 9.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Sep 2016 23:47:32 +0200

libdaemon-generic-perl (0.84-1) unstable; urgency=low

  * Initial upload to Debian.
    Closes: #745436
  * An earlier release was already in Ubuntu.
    This upload adds the missing dependencies mentioned in Launchpad.
    LP: #1093429

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Apr 2014 20:21:34 +0200
